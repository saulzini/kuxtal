﻿using UnityEngine;
using System.Collections;

public class Nivel3 : MonoBehaviour {

	public int objetosRecogidos=0;
	public int objectosARecoger=2;

	public int objetosDesconectados=0;
	public int objectosADesconectar=2;

	public bool dispostivo=false;
	
	void UsarPC(){
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);	
		RaycastHit hit;
		
		
		if (Input.GetButtonDown ("Fire1")) {
			
			if (Physics.Raycast (ray, out hit)) {
				
				if (hit.transform.tag == "Desconectar") {
					
					objetosDesconectados++;
					Destroy(hit.transform.gameObject);

				}
				
				
			
				if (hit.transform.tag == "Objeto") {
					
					objetosRecogidos++;
					Destroy(hit.transform.gameObject);

					
				}
				
				
				
			}
			
		}
		
		
		
		
	}

	void UsarDispositivo(){
		
		Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
		//Ray ray1 = new Ray (Input.GetTouch (0).position, transform.forward);
		
		if (Input.touchCount > 0) {
			RaycastHit hit;
			
			if (Physics.Raycast (ray, out hit)) {
				
				if (Physics.Raycast (ray, out hit)) {
					
					if (hit.transform.tag == "Desconectar") {
						
						objetosDesconectados++;
						Destroy(hit.transform.gameObject);

					}
					
					
					
					if (hit.transform.tag == "Objeto") {
						
						objetosRecogidos++;
						Destroy(hit.transform.gameObject);
					
						
					}
					
					
					
				}
				
				
			}
			
		}
		
		
		
		
	}



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (dispostivo == false) {
			UsarPC();
		
		}
		if (dispostivo == true) {
			UsarDispositivo();
		}


	}
}
