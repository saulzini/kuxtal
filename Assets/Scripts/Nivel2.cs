﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class Nivel2 : MonoBehaviour {

	private VentanYPuerta scriptVentanaPuerta;

	private GameObject[] ObjetosVentana;
	private GameObject[] ObjetosPuerta;

	// Use this for initialization
	public int Ventanas= 2;
	public int Puertas = 1;

	public GameObject nivelAgua ;
	public float velocidad = 10f;
	private GameObject personaje;
	public int tiempo = 5;


	public bool playing;
	private int contador;
	public float timeLeft;
	public Text timeLeftText;
	public static bool pausa;
	public GameObject ventanaPausa;


	public int ObjetosArecoger;
	private int objetosRecogidos;
	public bool Dispositivo;

	public void startGame()
	{
		Movimiento.vida = 3;
		ventanaPausa.SetActive (false);
		//GetComponent<AudioSource>().Play();
		StartCoroutine(Spawn());
		playing = true;
		pausa = false;
		Time.timeScale = 1; //Continua el juego
		AdministradorNiveles.setNivel(2);
		AdministradorNiveles.setNivelPrevio (2);



	}
	
	public void enPausa(){
		pausa = true;
		Time.timeScale = 0; //Parar el juego
		//GetComponent<AudioSource>().Pause();
		ventanaPausa.SetActive (true);
	}
	
	public void Reiniciar(){
		Application.LoadLevel ("cargandoJuego");
		
	}
	public void Continuar(){
		pausa = false;
		ventanaPausa.SetActive (false);
		//GetComponent<AudioSource>().Play();
		Time.timeScale = 1; //Continua el juego
		
	}
	
	public void menu(){
		Application.LoadLevel ("Menu");
	}


	// Update is called once per frame
	void FixedUpdate () {
		if (playing)
		{
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0)
			{
				timeLeft = 0;
			}
			timeLeftText.text = ""+Mathf.RoundToInt(timeLeft);
		}
		
	}


	IEnumerator girar(int indice){
		ObjetosVentana[indice].GetComponent<VentanYPuerta>().abierto = false;
		Ventanas--;
		yield return new WaitForSeconds(tiempo);
	
		ObjetosVentana [indice].GetComponent<VentanYPuerta> ().abierto = true;

		Ventanas++;

	}

	IEnumerator girarPuerta(int indice){
		
		ObjetosPuerta[indice].GetComponent<VentanYPuerta>().abierto = false;

		Puertas--;
		yield return new WaitForSeconds(tiempo);
		
		ObjetosPuerta[indice].GetComponent<VentanYPuerta>().abierto = true;

		
		Puertas++;
		
	}


	void Start () {
		personaje = GameObject.FindGameObjectWithTag ("Personaje");

		if (ObjetosVentana == null) {
			ObjetosVentana = GameObject.FindGameObjectsWithTag ("Ventana");
		}

		if (ObjetosPuerta == null) {
			ObjetosPuerta = GameObject.FindGameObjectsWithTag ("Puerta");
		}



		startGame ();
	}


	IEnumerator Spawn()
	{
		while (Movimiento.vida > 0)
		{
			if(timeLeft == 0 && Movimiento.estadoGanar == false)
				Movimiento.estadoGanar = true;
			
			if(Movimiento.estadoGanar)
			{
				AdministradorNiveles.setNivel(1); //CAMBIAR A 3
				Application.LoadLevel ("Ganar"); //ventana de ganar
				break;
			}
			
			yield return new WaitForSeconds(1.0f);
		}
		
		if (Movimiento.estadoGanar == false) {
			yield return new WaitForSeconds(1.5f);
			Application.LoadLevel ("Perder"); //ventana de perder
		}
		
		playing = false;
		Movimiento.estadoGanar = false;
	}


	void UsarDispositivo(){

		Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
	
		
		if (Input.touchCount > 0) {
			RaycastHit hit;
			
			if (Physics.Raycast (ray, out hit)) {

				if (hit.transform.tag == "Ventana") {
					
					int numero = System.Array.IndexOf (ObjetosVentana, hit.transform.gameObject);
					
					
					
					if (ObjetosVentana[numero].GetComponent<VentanYPuerta>().abierto == true) {
						StartCoroutine (girar (numero));
						
						
					}
					
					
				}
				
				
				if (hit.transform.tag == "Puerta") {
					
					int numero = System.Array.IndexOf (ObjetosPuerta, hit.transform.gameObject);
					
					
					
					if (ObjetosPuerta[numero].GetComponent<VentanYPuerta>().abierto == true) {
						StartCoroutine (girarPuerta (numero));
						
					}
					
					
				}	
				
				if (hit.transform.tag == "Objeto") {

					objetosRecogidos++;
					Destroy(hit.transform.gameObject);
					
				}


			}
			
		}




	}

	void UsarPC(){

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);	
		RaycastHit hit;
		
		
		if (Input.GetButtonDown ("Fire1")) {
			
			if (Physics.Raycast (ray, out hit)) {
				
				if (hit.transform.tag == "Ventana") {

					int numero = System.Array.IndexOf (ObjetosVentana, hit.transform.gameObject);


				
					if (ObjetosVentana[numero].GetComponent<VentanYPuerta>().abierto == true) {
						StartCoroutine (girar (numero));

						
					}
					
					
				}
				
				
				if (hit.transform.tag == "Puerta") {

					int numero = System.Array.IndexOf (ObjetosPuerta, hit.transform.gameObject);
					
					
					
					if (ObjetosPuerta[numero].GetComponent<VentanYPuerta>().abierto == true) {
						StartCoroutine (girarPuerta (numero));
						
					}

					
				}	
				
				if (hit.transform.tag == "Objeto") {
					
					objetosRecogidos++;
					Destroy(hit.transform.gameObject);
					
				}
				
				
				
			}
			
		}



	
	}

	// Update is called once per frame
	void Update () {


		if (Dispositivo == false) {
			UsarPC();
		}

		if (Dispositivo == true) {
			UsarDispositivo();
		
		}


		//Subir el nivel del agua
		if (  (Ventanas > 0 || Puertas > 0) &&  !pausa ) {

			nivelAgua.transform.Translate (0, velocidad, 0, Space.World);
			
		}
	

		//Si perdiste el juego 
		// el pesonaje se ahogo
		if (nivelAgua.transform.position.y > personaje.transform.position.y) {
			Application.LoadLevel ("Perder");
			playing = false;
			Movimiento.estadoGanar = false;
		}
	}

}
