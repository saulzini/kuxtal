﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Humo : MonoBehaviour {
	
	public float velocidad = 0.05f;
	private Movimiento mover;
	
	
	void FixedUpdate () {
		transform.Translate(  -velocidad, 0,0 ,Space.World);
	}
	
	void OnParticleCollision(GameObject other)
	{
		Spawn ();
		GameObject aux = GameObject.FindGameObjectWithTag("Personaje");
		mover = (Movimiento)aux.GetComponent (typeof(Movimiento));


		if (Movimiento.agachado == false) {

		//	Debug.Log (Movimiento.vida);
			mover.colisionHumo();
		//	Debug.Log (Movimiento.vida);
		}
	}
	
	
	public IEnumerator Spawn() 
	{
		yield return new WaitForSeconds (2f);
	}
	
}
