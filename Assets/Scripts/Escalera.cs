﻿using UnityEngine;
using System.Collections;

public class Escalera : MonoBehaviour {


	public GameObject pisoContrario;
	private GameObject personaje;
	public GameObject camara;
	private Vector3 pisoContrarioparaCamara;
	// Use this for initialization



	void Start () {
		personaje = GameObject.FindGameObjectWithTag ("Personaje");
		pisoContrarioparaCamara = new Vector3(0,pisoContrario.transform.position.y,0);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Personaje") {

			personaje.transform.position = pisoContrario.transform.position ;
			camara.transform.position = pisoContrarioparaCamara;
		}
	}
}
