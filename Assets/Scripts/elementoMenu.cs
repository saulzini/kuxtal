﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class elementoMenu : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {
	
	// Use this for initialization
	public Button boton;
	public Sprite imagen1;
	public Sprite imagen2;
	public Text textoBoton;


	public void OnPointerEnter (PointerEventData eventData) 
	{
     	boton.GetComponent<AudioSource>().Play();
		textoBoton.color = new Color32 (0, 0, 0, 255);
		boton.image.sprite = imagen2;
	}
	
	public void OnPointerExit (PointerEventData eventData) 
	{
		textoBoton.color = new Color32 (255, 255, 255,255);
		boton.image.sprite = imagen1;
	}
}
