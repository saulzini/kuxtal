﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuGanar : MonoBehaviour {
	public Image imageMenu ;
	private int nivelAux;
	public Text retroalimentacion;
	private string palabra1, palabra2;

	void Start () {

		if (Movimiento.vida == 3) {
			imageMenu.sprite = Resources.Load<Sprite> ("3Stars");
			palabra1 = "correctamente";
			palabra2 = "Sigue avanzando";
		} else if (Movimiento.vida == 2) {
			imageMenu.sprite = Resources.Load<Sprite> ("2Stars");
			palabra1 = "muy bien";
			palabra2 = "Mejora tu aprendizaje";
		} else if (Movimiento.vida == 1) {
			imageMenu.sprite = Resources.Load<Sprite> ("1Star");
			palabra1 = "bien";
			palabra2 = "Intenta mejorar"; 
		}

		if(AdministradorNiveles.getNivelPrevio() == 1)
			retroalimentacion.text = "Actuaste "+palabra1 +" ante un incendio ¡Bien hecho! "+palabra2;
		else if(AdministradorNiveles.getNivelPrevio() == 2)
			retroalimentacion.text = "Actuaste "+ palabra1 +" ante una inundación ¡Bien hecho! "+palabra2;
		else if (AdministradorNiveles.getNivelPrevio() == 3)
			retroalimentacion.text = "Actuaste "+ palabra1 +"correctamente ante un sismo ¡Bien hecho! "+palabra2;


	}
}
