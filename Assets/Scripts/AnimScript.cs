﻿using UnityEngine;
using System.Collections;

public class AnimScript : MonoBehaviour {

	public float maxSpeed = 10f;
	public bool facingLeft= true;

	Animator anim ;
	// Use this for initialization



	void Start () {
		anim = GetComponent<Animator> ();



	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float move= Input.GetAxis("Horizontal");


		anim.SetFloat ("speed", Mathf.Abs (move));

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (move * maxSpeed, GetComponent<Rigidbody2D> ().velocity.y);

		if (move > 0 && facingLeft == true) {
			flip ();
		
		} else if (move < 0 && !facingLeft) {
		
			flip();
		}
	}

	void flip(){

		facingLeft= !facingLeft;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;

		transform.localScale = theScale;
	}

}
