﻿using UnityEngine;
using System.Collections;

public class TrianguloDeVida : MonoBehaviour {

	public GameObject LugarDerecha;
	public GameObject LugarIzquierda;

	public GameObject objeto;

	public float tiempo=5;

	private GameObject auxDerecha;
	private GameObject auxIzquierda;


	private bool instancia=false;

	private Movimiento personajeScript;

	IEnumerator Instanciar(){

		auxDerecha= Instantiate (objeto, LugarDerecha.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;
		auxIzquierda= Instantiate (objeto, LugarIzquierda.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;

		//Tiempo para dormir haasta que llegue a tocar el piso 
		// el techo que cae
		yield return new WaitForSeconds (2);
		//validar si pego
		personajeScript = GameObject.FindGameObjectWithTag ("Personaje").GetComponent<Movimiento>();
		
		if (personajeScript.trianguloVidaVivo == false) {
			//quitar vida al personaje
			//print("Fallo");

		}
		if (personajeScript.trianguloVidaVivo == true) {
			//se salvo
			//print("Salve");
		}


		yield return new WaitForSeconds (tiempo);

	

		instancia = false;


	}

	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame
	void Update () {
		if (instancia == false) {
			instancia=true;
			StartCoroutine (Instanciar ());	
		}			
	}
}
