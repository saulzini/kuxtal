﻿using UnityEngine;
using System.Collections;

public class AdministradorNiveles : MonoBehaviour {

	/*NIVEL*/
	private static int nivel; 
	private static int nivelPrevio;


	public static int getNivel(){
		return nivel;
	}
	
	public static void setNivel(int nivelAux){
		nivel = nivelAux;
	}

	public static int getNivelPrevio(){
		return nivelPrevio;
	}
	
	public static void setNivelPrevio(int nivelAux){
		nivelPrevio = nivelAux;
	}


	public void Nivel1(){
		nivel = 1;
		nivelPrevio = 1;
		Application.LoadLevel ("cargandoJuego");
	}

	public void Nivel2(){
		nivel = 2;
		nivelPrevio = 2;
		Application.LoadLevel ("cargandoJuego");
	}

	public void Nivel3(){
		nivel = 3;
		nivelPrevio = 3;
		Application.LoadLevel ("cargandoJuego");
	}

}
