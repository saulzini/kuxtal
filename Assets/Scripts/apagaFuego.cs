﻿using UnityEngine;
using System.Collections;

public class apagaFuego : MonoBehaviour {
void OnParticleCollision(GameObject other){
		Debug.Log ("ya esta apagando "+ other.name);
		if (other.tag == "flameApaga") {
			//other.GetComponent<ParticleSystem>().emissionRate -=0.6f;
			other.GetComponent<ParticleSystem>().startLifetime -= 0.6f;
			if(other.GetComponent<ParticleSystem>().startLifetime <= 0){
				Destroy(other);
				Debug.Log ("Fuego destruido");
			}
		}
	}
}
