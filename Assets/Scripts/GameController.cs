﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	
	public ParticleSystem humo;
	public bool playing;
	private int contador;
	public float timeLeft;
	public Text timeLeftText;
	public static bool pausa;
	public GameObject ventanaPausa;
	public Text retroalimentacion;
	public Image mensajeImagen;
	//public MoveFlames mFlames;

	
		private float maxWidth;

	// Use this for initialization
	void Start () {
			startGame ();
	}
	
	public void startGame()
	{
		Movimiento.vida = 3;
		ventanaPausa.SetActive (false);
		GetComponent<AudioSource>().Play();
		playing = true;
		StartCoroutine(Spawn());
		pausa = false;
		Time.timeScale = 1; //Continua el juego
		//mFlames.empieza ();
	}


	public void enPausa(){
		pausa = true;
		Time.timeScale = 0; //Para el juego
		GetComponent<AudioSource>().Pause();
		ventanaPausa.SetActive (true);
	}

	public void Reiniciar(){
		Application.LoadLevel ("cargandoJuego");
		
	}
	public void Continuar(){
		pausa = false;
		ventanaPausa.SetActive (false);
		GetComponent<AudioSource>().Play();
		Time.timeScale = 1; //Continua el juego
		AdministradorNiveles.setNivel(1);
		AdministradorNiveles.setNivelPrevio (1);

	}

	public void menu(){
		Application.LoadLevel ("Menu");
	}

	
	// Update is called once per frame
	void FixedUpdate () {
		if (playing)
		{
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0)
			{
				timeLeft = 0;
			}
			timeLeftText.text = ""+Mathf.RoundToInt(timeLeft);
		}

	}
	
	IEnumerator Spawn()
	{
		while (Movimiento.vida > 0)
		{
			if(timeLeft == 0 && Movimiento.estadoGanar == false)
				Movimiento.estadoGanar = true;

			if(Movimiento.estadoGanar == false)
			{
				Quaternion spawnRotation = Quaternion.identity;
				if(Movimiento.estadoHumo){
				Instantiate(humo,new Vector3(transform.position.x + 7, transform.position.y + 10, transform.position.z),spawnRotation);
				}

			}else
			{
				mensajeImagen.sprite = Resources.Load<Sprite> ("bienHecho");
				retroalimentacion.text = "¡Bien hecho!";
				AdministradorNiveles.setNivel(2);
				Application.LoadLevel ("Ganar"); //ventana de ganar
				break;
			}

			yield return new WaitForSeconds(1.0f);
		}
	
		if (Movimiento.estadoGanar == false) {

			mensajeImagen.sprite = Resources.Load<Sprite> ("error");
			retroalimentacion.text = "¡Woops!";
			yield return new WaitForSeconds(1.5f);

			Application.LoadLevel ("Perder"); //ventana de perder
		}

		playing = false;
		Movimiento.estadoHumo = false;
		Movimiento.estadoTutorial1 = false;
		Movimiento.estadoGanar = false;

	}

	public bool getPlaying(){
		return this.playing;
	}
}