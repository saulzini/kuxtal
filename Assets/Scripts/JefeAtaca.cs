﻿using UnityEngine;
using System.Collections;

public class JefeAtaca : MonoBehaviour {
	
	// Use this for initialization
	
	Animator anim ;
	GameObject Personaje;
	int Accion;
	public GameObject  Objeto;
	public GameObject objetoLanzado;
	public GameObject objetoLanzadoIzquierdo;
	public int cantidad;
	public GameObject objetoLanzadoSupremo;
	public  float distancia = 20f;
	
	private GameObject aux;
	public float tiempoObjeto = 3;
	
	void Start () {
		anim = GetComponent<Animator> ();
		Personaje =  GameObject.FindGameObjectWithTag("Personaje");
		StartCoroutine (Atacar ());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		
	}
	
	void  atacarCero(){
		
		float a = Personaje.transform.position.x;
		float b = transform.position.x;
		
		if (Mathf.Abs (a - b) > distancia) {
			
			transform.position = new Vector3(Personaje.transform.position.x,transform.position.y,transform.position.z);
			
		}
	}
	void atacarUno(){
		
		for (int i=0; i<cantidad; i++) {
			Vector3 bolasAtaque = new Vector3 (objetoLanzado.transform.position.x, objetoLanzado.transform.position.y,Personaje.transform.position.z); 
			
			aux = Instantiate (Objeto, bolasAtaque, Personaje.transform.rotation) as GameObject;
			Destroy(aux,tiempoObjeto);
		}
		
		
	}
	
	void atacarDos(){
		for (int i=0; i<cantidad; i++) {
			Vector3 bolasAtaque = new Vector3 (objetoLanzadoIzquierdo.transform.position.x, objetoLanzadoIzquierdo.transform.position.y,Personaje.transform.position.z); 
			
			aux = Instantiate (Objeto, bolasAtaque, Personaje.transform.rotation) as GameObject;
			Destroy(aux,tiempoObjeto);
			
		}
		
		
	}
	
	void atacarTres(){
		
		for (int i=0; i<cantidad; i++) {
			Vector3 bolasAtaque = new Vector3 (Personaje.transform.position.x+(0.17f*i*10), objetoLanzadoSupremo.transform.position.y,Personaje.transform.position.z); 
			aux = Instantiate (Objeto, bolasAtaque, Personaje.transform.rotation) as GameObject;
			Destroy(aux,tiempoObjeto);
			
		}
		
	}
	IEnumerator Atacar(){
		
		while (true) {
			Accion = Random.Range (0, 4);
			print("Accion: "+Accion);
			if (Accion == 0) {
				anim.SetInteger ("Ataque", 0);
				
			} else if (Accion == 1) {
				//1
				anim.SetInteger ("Ataque", 1);
				
				
				
				
			} else if (Accion == 2) {
				//2
				anim.SetInteger ("Ataque", 2);
				
				
				
				
			} else if (Accion == 3) {
				//3
				//golpe con ambas manos
				
				anim.SetInteger ("Ataque", 3);
				
				
			}
			yield return new WaitForSeconds (3);
			
		}
	}
	
}