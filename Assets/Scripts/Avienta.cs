﻿using UnityEngine;
using System.Collections;

public class Avienta : MonoBehaviour {

	public GameObject ball;




	private bool playing;
	
	private float maxWidth;

	void Start () {		
		StartCoroutine(Spawn());
	}

	IEnumerator Spawn(){
		Vector3 SpawnPosition = new Vector3 (transform.position.x+2, transform.position.y+3.0f, transform.position.z-5);
		Quaternion spawnRotation = Quaternion.identity;
	
		Instantiate (ball, SpawnPosition, spawnRotation);
	
		yield return new WaitForSeconds (Random.Range (0.8f, 1.5f));
	}
}
