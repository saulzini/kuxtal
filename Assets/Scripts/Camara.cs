﻿using UnityEngine;
using System.Collections;

public class Camara : MonoBehaviour {
	public GameObject personajePrincipal;
	public float smooth = 2.0F;
	public float tiltAngle = 30.0F;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	
		transform.position = new Vector3 (personajePrincipal.transform.position.x, transform.position.y, transform.position.z);

			float prueba = Input.acceleration.x * tiltAngle;
			//float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;
			//float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
			Quaternion target = Quaternion.Euler(0, prueba, 0);
			transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);

	}
}



