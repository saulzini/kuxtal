﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour {
	
	//Variables para el swipe 
	public float minSwipeDistY;
	public float salto; 
	private Vector2 startPos;
	public Image image ;
	public static int vida = 3;
	public bool Dispositivo = false;
	public Text retroalimentacion;
	public Image mensajeImagen;

	
	//public Text scoreText;
	
	/// <summary>
	/// variables para la animacion
	/// 	/// </summary>
	
	public float maxSpeed = 10f;
	public bool facingLeft= true;
	

	
	public bool extintor = false;

	public static bool agachado=false;



	/// <summary>
	/// T///Estados
	/// </summary>
	public GameObject activarHumo;
	public static bool estadoHumo = false;
	public static bool estadoTutorial1 = false;
	public static bool estadoGanar = false;
	public static bool estadoJefe = false;


	private float move;

	Animator anim ;

	public bool trianguloVidaVivo=true;

	void Start () {
		anim = GetComponent<Animator> ();
		
		
		
	}

	void Extintor(){
		
		if (Dispositivo == false) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			//Ray ray = new Ray(Input.GetTouch (0).position, transform.forward);
			
			//if (Input.touchCount > 0) {
			RaycastHit hit;
			
			if (Input.GetButtonDown ("Fire1")) {
				if (Physics.Raycast (ray, out hit, 1000)) {
					
					if (hit.transform.tag == "Extintor") {
						Destroy (hit.transform.gameObject);
						anim.SetBool ("extintor", true);
						extintor = true;
					}
				}
			}
		} else {
			
			/*
			Para moviles

		 */
			
			Ray ray1 = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
			//Ray ray1 = new Ray (Input.GetTouch (0).position, transform.forward);
			
			if (Input.touchCount > 0) {
				RaycastHit hit1;
				
				if (Physics.Raycast (ray1, out hit1, 1000)) {
					
					if (hit1.transform.tag == "Extintor") {
						Destroy (hit1.transform.gameObject);
						anim.SetBool("extintor",true);
						extintor=true;
					}
				}
				
			}
			
		}
		
		
	}
	
	
	// Update is called once per frame
	// Update is called once per frame
	void FixedUpdate () {
		
		
		
		if (Dispositivo == true) {
			move= Input.acceleration.normalized.x*5;	
		}
		if (Dispositivo == false) {
			move= Input.GetAxis("Horizontal");
		}
		
		//f
		
		anim.SetFloat ("speed", Mathf.Abs (move));
		
		GetComponent<Rigidbody> ().velocity = new Vector3 (move * maxSpeed, GetComponent<Rigidbody> ().velocity.y, GetComponent<Rigidbody> ().velocity.z);
		
		if (move > 0 && facingLeft == true) {
			flip();
			
		} else if (move < 0 && !facingLeft) {
			
			flip();
		}
		
		if (Dispositivo == false) {
			///Validar lo del disparo
			if (Input.GetButton ("Fire1") && extintor == true) {
				
				anim.SetBool ("atacar", true);
				anim.SetFloat ("speed", 0);
				GetComponent<Rigidbody> ().velocity = new Vector3 (0, GetComponent<Rigidbody> ().velocity.y, GetComponent<Rigidbody> ().velocity.z);
				
			}
			if (Input.GetButton ("Fire1") == false && extintor == true) {
				
				anim.SetBool ("atacar", false);
				
			}
		}
		if (Dispositivo == true) {
			///////////////////////////////////////////////////////////
			/// Validar lo del disparo (movil)
			/// /////////////////////////////////////
			if (Input.touchCount> 0 && extintor == true) {
				Debug.Log("dasd");
				anim.SetBool ("atacar", true);
				anim.SetFloat ("speed", 0);
				GetComponent<Rigidbody> ().velocity = new Vector3 (0, GetComponent<Rigidbody> ().velocity.y, GetComponent<Rigidbody> ().velocity.z);
				
			}
			if (Input.touchCount<= 0 && extintor == true) {
				Debug.Log("dasd");
				anim.SetBool ("atacar", false);
				
			}
		}
		
		//////////////////////////////////////
		/// ///////////////////////////////
		/// 
		/// /////////////////////////////
		/// 
		
		
		
		swipes ();
		Extintor ();
	}
	
	
	void flip(){

		facingLeft= !facingLeft;
		anim.transform.Rotate(0, 180, 0);
		//transform.eulerAngles = new Vector3(0, 180, 0);


	
	}



	void swipes(){
		
		
		//#if UNITY_ANDROID
		if (Input.touchCount > 0) 
			
		{
			
			Touch touch = Input.touches[0];
			
			
			
			switch (touch.phase) 
				
			{
				
			case TouchPhase.Began:
				
				startPos = touch.position;
				
				break;
				
				
				
			case TouchPhase.Ended:
				
				float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
				
				if (swipeDistVertical > minSwipeDistY) 
					
				{
					
					float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
					
					if (swipeValue > 0){//up swipe
						
						anim.SetBool ("agachado", false);
						agachado=false;

					}
					
					else {//down swipe
						//Shrink ();
						agachado=true;
						anim.SetBool ("agachado", true);
					}
					
				}
				
				break;
			}
		}
		
		
	}
	
	
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag != "Piso" && other.gameObject.tag != "Escalera" 
		    && other.gameObject.tag !="LugarMatarse" && other.gameObject.tag !="LugarSalvarse") {

			switch (other.gameObject.tag) {
				
			case "Humo":

				estadoTutorial1 = false;
				estadoHumo = true;
				estadoJefe = false;
				break;

			case "Tutorial1":
				estadoHumo = false;
				estadoTutorial1 = true;
				estadoJefe=false;
				break;

			case "MensajeN1_1":
				mensajeImagen.sprite = Resources.Load<Sprite> ("advertencia");
				retroalimentacion.text = "Si hay humo agachate y gatea, es peligroso respirarlo.";
				break;

			case "MensajeN1_2":
				mensajeImagen.sprite = Resources.Load<Sprite> ("advertencia");
				retroalimentacion.text = "Utiliza el extintor para apagar las pequeñas llamas que bloquean el camino";
				break;

			case "MensajeN1_3":
				mensajeImagen.sprite = Resources.Load<Sprite> ("advertencia");
				retroalimentacion.text = "Tranquilo falta poco para que lleguen los bomberos, resiste ante el enemigo";
				break;

			case "MensajeN1_4":
				mensajeImagen.sprite = Resources.Load<Sprite> ("advertencia");
				retroalimentacion.text = "¡Oh no! la salida esta bloqueada por más fuego";
				break;

			case "Jefe":
				estadoHumo=false;
				estadoGanar=false;
				estadoTutorial1=false;
				estadoJefe=true;

				break;
			case "Ganar":
				estadoHumo = false;
				estadoTutorial1 = false;
				estadoGanar = true;
				estadoJefe=false;
				break;
			
		

			default:
				StartCoroutine(mePego());
				break;
				
			}
		}


	



	}






	void OnTriggerStay(Collider other ) {


		if (other.gameObject.tag != "Piso" && other.gameObject.tag != "Escalera") {
			print (trianguloVidaVivo);
			if ( other.gameObject.tag == "LugarSalvarse" ){
				trianguloVidaVivo=true;

			}

			else if ( other.gameObject.tag == "LugarMatarse" ){
				trianguloVidaVivo=false;
				
			}

			else  if (other.gameObject.tag == "LugarMatarse"  && other.gameObject.tag == "LugarSalvarse"  ){
				trianguloVidaVivo=false;

			}

		}

	}

	void OnCollisionEnter(Collision other)
	{ 
		//Debug.Log ("Ya me pego 1");
		if (other.gameObject.tag == "flame" || other.gameObject.tag=="flameApaga"  ) {
			StartCoroutine (mePego ());
			mensajeImagen.sprite = Resources.Load<Sprite> ("error");
			retroalimentacion.text = "¡Escapa del fuego! núnca lo enfrentes";
		}

	

	}

	public IEnumerator mePego(){
		if (anim.GetBool ("danio") == false) {
			anim.SetBool ("danio", true);
			//Debug.Log ("Soy invencible we");
			restarVida ();
			yield return new WaitForSeconds (2.0f);
			anim.SetBool ("danio", false);
		}		
	}
	
	public void colisionHumo(){
		StartCoroutine (mePego ());
		mensajeImagen.sprite = Resources.Load<Sprite> ("error");
		retroalimentacion.text = "¡Agachate! es peligroso respirar el humo";
	}
	

	public void restarVida()
	{
		image = GameObject.Find ("Vida").GetComponent<Image> ();
		if (vida > 0)
			vida--;
		
		if (vida == 3) {
			image.sprite = Resources.Load<Sprite> ("vida1");
		} else if (vida == 2) {
			image.sprite = Resources.Load<Sprite> ("vida2");
		} else if (vida == 1) {
			image.sprite = Resources.Load<Sprite> ("vida3");
		} else if (vida == 0) {
		    image.sprite = Resources.Load<Sprite> ("vida4");
			StartCoroutine(fantasma());
		}
	}

	IEnumerator fantasma(){
		anim.SetBool ("muere", true);
		GetComponent<Rigidbody> ().velocity = new Vector3 (0, GetComponent<Rigidbody> ().velocity.y + 10, 0);
		yield return new WaitForSeconds (10.0f);
	}
}
