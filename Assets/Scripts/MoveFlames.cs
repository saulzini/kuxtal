﻿using UnityEngine;
using System.Collections;

public class MoveFlames : MonoBehaviour {
  

	public float velocidad = 0.05f;
	private GameController game;

 
	// Use this for initialization
    void Start () {
    }


    void FixedUpdate () {
	
		GameObject aux = GameObject.Find ("Game");
		game = (GameController)aux.GetComponent (typeof(GameController));
		//Debug.Log (game.playing);
		if(game.playing == true){
			transform.Translate(  velocidad, 0,0 ,Space.World);
		}
	}
}
	