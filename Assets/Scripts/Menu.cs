﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Menu : MonoBehaviour {
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;
	
	public void Iniciar(){
		Application.LoadLevel ("SeleccionNivel");
	}

	public void Reiniciar(){
		AdministradorNiveles.setNivel(AdministradorNiveles.getNivelPrevio());
		Application.LoadLevel ("cargandoJuego");
	}

	public void SiguienteNivel(){
		AdministradorNiveles.setNivelPrevio (AdministradorNiveles.getNivel());
		Application.LoadLevel ("cargandoJuego"); //cambiar	
	}
	
	public void menu(){
		Application.LoadLevel ("Menu");
	}

	public void Instrucciones(){
		Application.LoadLevel ("Instrucciones"); 	
	}

	public void Historia(){
		Application.LoadLevel ("Historia"); 
	}
	
	public void Salir(){
		Application.Quit();
	}
}
